# rpi_utils
I created this repo to implement some functionality to my newly ordered [SH1106 OLED display](https://www.amazon.de/s?k=sh1106+oled+display&crid=3VW70IX8GHUK4&sprefix=SH1106%2Caps%2C83&ref=nb_sb_ss_ts-doa-p_1_6) which is connected to my Raspberry Pi 4. Currently two pages are being drawn on the display. The first is a "General" page which shows: **device IP, CPU, memory usage, CPU temperature and uptime**. The second "Deluge" page gives me information aboud my torrent server: **up and download speed (kBps), currently seeded and downloading torrent count**.

![rpi_utils_display](/uploads/9a4151628e5c80dd7190c2536047183f/rpi_utils_display.gif)

# Installation and Usage
After checking out the repo ```cd``` to the root and:
```sh
poetry install
poetry shell
```

Instead of using  ```poetry shell``` you can always find and use the created ```venv```, then:
```sh
python main.py
```

If you want it to work after your ```SSH``` session is ended to the Raspberry Pi you can either use ```nohup```:
```sh
nohup python main.py &
```

or automatically start the script on startup:
```sh
sudo crontab -e
```

and add the following lines to the end of the file:
```sh
sudo python3 $(PATH_TO_REPO)/main.py
```

# TODOs:
The script should check if an instance is already running at start (I don't know what happens when you start two instances :))