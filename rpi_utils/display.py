import subprocess
import time
from typing import List

import psutil
from luma.core.interface.serial import i2c
from luma.core.render import canvas
from luma.oled.device import sh1106
from PIL.ImageDraw import ImageDraw

from rpi_utils.deluge_utils import DelugeHandler
from rpi_utils.plex_utils import PlexHandler

class DisplayHandler:
    """
    Class that visualize General device (IP, CPU, memory usage, temperature, uptime) Deluge (number of
    seeding/downloading torrents, upload and download speed in kBps) and Plex (session count) info on a SH1106 OLED
    display (on a specific I2C port and address).
    """
    I2C_PORT = 1
    I2C_ADDRESS = 0x3C

    def __init__(self) -> None:
        self.deluge_handler = DelugeHandler()
        self.plex_handler = PlexHandler()
        self.device = sh1106(i2c(port=DisplayHandler.I2C_PORT, address=DisplayHandler.I2C_ADDRESS))

        self.frame_cntr = 0
        self.display_cntr = 0

    def update(self) -> None:
        """
        Updates the OLED screen connected to the Raspberry pi. For 5 updates it shows the General info then for 5
        updates it shows the Deluge and Plex info. Before every update it clears the screen.
        """
        if self.frame_cntr % 5 == 0:
            self.display_cntr += 1

        with canvas(self.device) as display:
            self.clear_display(display)

            if self.display_cntr % 2 == 1:
                DisplayHandler.draw_general_stats(display)
            else:
                DisplayHandler.draw_deluge_and_plex_stats(display, self.deluge_handler, self.plex_handler)

        self.frame_cntr += 1

    def clear_display(self, display: ImageDraw) -> None:
        """
        Clears the display.
            Parameters:
                display (ImageDraw): The OLED display object
        """
        display.rectangle(self.device.bounding_box, fill="black")

    @staticmethod
    def draw_general_stats(display: ImageDraw) -> None:
        """
        Creates the General stats (IP, CPU, memory, temperature, uptime) window for the display then draws it.
            Parameters:
                display (ImageDraw): The OLED display object
        """
        stats = [
            '       GENERAL:',
            '',
            DisplayHandler.get_ip_message(),
            DisplayHandler.get_cpu_message(),
            DisplayHandler.get_mem_message(),
            DisplayHandler.get_temperature_message(),
            DisplayHandler.get_uptime_message(),
        ]

        DisplayHandler.position_stats_on_display(stats, display)

    @staticmethod
    def draw_deluge_and_plex_stats(display: ImageDraw, deluge_handler: DelugeHandler,
                                   plex_handler: PlexHandler) -> None:
        """
        Creates the Deluge (number of seeding/downloading torrents, upload and download speed in kBps) and Plex
        (session count) stats window for the display then draws it.
            Parameters:
                display (ImageDraw): The OLED display object
                deluge_handler (DelugeHandler): Deluge handler object
                plex_handler (PlexHandler): Plex handler object
        """
        deluge_stats = deluge_handler.get_stats()
        plex_stats = plex_handler.get_stats()

        stats = [
            '       DELUGE:',
            '',
            f'Up({deluge_stats["seeding"]}): {deluge_stats["upload_speed_bps"]/1024.0:.2f}kBps',
            f'Down({deluge_stats["downloading"]}): {deluge_stats["download_speed_bps"]/1024.0:.2f}kBps',
            '',
            '         PLEX:',
            f'Sessions: {plex_stats["session_count"]}',
        ]

        DisplayHandler.position_stats_on_display(stats, display)

    @staticmethod
    def position_stats_on_display(stats: List[str], display: ImageDraw) -> None:
        """
        Draws text on the OLED screen from a list line by line.
            Parameters:
                stats (List[str]): List that contains the text to draw (line by line)
                display (ImageDraw): The OLED display object
        """
        for idx, stat in enumerate(stats):
            display.text((0, 8 * idx), f"{stat}", fill="white")

    @staticmethod
    def get_ip_message() -> str:
        """
        Gets the device's IP address.
            Returns:
                (str): The device's IP address
        """
        return DisplayHandler.run_shell_command("hostname -I | cut -d' ' -f1 | awk '{printf \"IP: %s\", $1}'")

    @staticmethod
    def get_cpu_message() -> str:
        """
        Gets the device's CPU load in percent.
            Returns:
                (str): The device's CPU load in percent
        """
        return f'Cpu: {psutil.cpu_percent()}%'

    @staticmethod
    def get_mem_message() -> str:
        """
        Gets the device's memory usage in percent.
            Returns:
                (str): The device's memory usage in percent
        """
        return f'Mem: {psutil.virtual_memory().percent:.1f}%'

    @staticmethod
    def get_temperature_message() -> str:
        """
        Gets the device's CPU temperature in °C.
            Returns:
                (str): The device's CPU temperature in °C
        """
        return f'Temp: {psutil.sensors_temperatures(fahrenheit=False)["cpu_thermal"][0].current:.1f}°C'

    @staticmethod
    def get_uptime_message() -> str:
        """
        Gets the device's uptime in a unique format. The format is: "Up: %DAYSdays, %H:%M:%S",
        E.G.: "Up: 12days, 02:23:12"
            Returns:
                (str): The device's uptime in a unique format.
        """
        elapsed_time_since_boot = int(time.time() - psutil.boot_time())

        days = elapsed_time_since_boot//86400
        time_string = time.strftime("%H:%M:%S", time.gmtime((elapsed_time_since_boot - (86400*days))))

        return f'Up: {days}days, {time_string}'

    @staticmethod
    def run_shell_command(cmd: str) -> str:
        """
        Executes a shell command and (utf-8) decodes the returned string.
            Parameters:
                cmd (str): The shell command to execute

            Returns:
                (str): The result of the shell command execution
        """
        return subprocess.check_output(cmd, shell=True).decode("utf-8")
