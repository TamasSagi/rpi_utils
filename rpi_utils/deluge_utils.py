import json
from pathlib import Path
from typing import Dict, List, TypedDict

from deluge_client import LocalDelugeRPCClient


class TorrentInfo(TypedDict):
    """
    Typed namespace for torrent info.
    """
    queued: int
    seeding: int
    downloading: int
    upload_speed_bps: float
    download_speed_bps: float


class TorrentStats(TypedDict):
    """
    Typed namespace for torrent statistics.
    """
    name: str
    state: str
    is_finished: bool
    active_time: int
    download_payload_rate: int
    upload_payload_rate: int
    ratio: float


class DelugeHandler:
    """
    Class to connect to the locally running Deluge daemon and read torrent statistics from it.
    """
    REQUIRED_RATIO = 1.2
    REQUIRED_SEED_TIME = 50*60*60  # 50 hours
    TORRENT_STATS_TO_QUERY = ['name', 'is_finished', 'ratio', 'active_time', 'state',
                              'download_payload_rate', 'upload_payload_rate']

    def __init__(self) -> None:
        self.client = LocalDelugeRPCClient()
        self.client.connect()

    def get_torrent_list(self, torrents_to_filter: Dict[str, List[str]] = None,
                         stats_to_query: List[str] = None) -> Dict[str, TorrentStats]:
        """
        Get torrent status based on a dict of torrent hashes and a list of status fields.
            Parameters:
                torrents_to_filter (Dict[str, List[str]]): Dictionary which can be used to filter torrents by hash
                stats_to_query (List[str]): List of strings which can be used to filter torrent statistics

            Returns:
                (Dict[str, TorrentStats]): A dictionary which keys are the torrent hashes and it's values are the
                                           content of "stats_to_query"
        """
        torrents_to_filter = {} if torrents_to_filter is None else torrents_to_filter
        stats_to_query = DelugeHandler.TORRENT_STATS_TO_QUERY if stats_to_query is None else stats_to_query

        return self.client.call('core.get_torrents_status', torrents_to_filter, stats_to_query)

    def get_stats(self) -> TorrentInfo:
        """
        Counts how many torrents are in downloading/seeding phase and what is the current down and upload speed (Bps).
            Returns:
                (TorrentInfo): Dictionary that contains the number of queued, seeding, downloading torrents, and
                               the current up and download speed in bytes per second.
        """
        torrents = self.get_torrent_list()
        queued = seeding = downloading = upload_speed_bps = download_speed_bps = 0

        for torrent_hash, torrent_values in torrents.items():
            if self.should_stop_seeding(torrent_hash, torrent_values):
                continue

            if torrent_values['state'].lower() == 'queued':
                queued += 1
            elif torrent_values['is_finished']:  # If not queued AND "is_finished"
                seeding += 1
            else:
                downloading += 1

            upload_speed_bps += torrent_values['upload_payload_rate']
            download_speed_bps += torrent_values['download_payload_rate']

        return {'queued': queued, 'seeding': seeding, 'downloading': downloading,
                'upload_speed_bps': upload_speed_bps, 'download_speed_bps': download_speed_bps}

    def should_stop_seeding(self, torrent_hash: str, torrent_values: TorrentStats) -> bool:
        """
        Checks if a torrent can be removed from the list of seeded torrents. If yes it is getting removed.
            Parameters:
                torrent_hash (str): Hash of the torrent file
                torrent_values (TorrentStats): Stats of the torrent file

            Returns:
                (bool): Torrent is removed or not
        """
        if (torrent_values['active_time'] > DelugeHandler.REQUIRED_SEED_TIME or
            torrent_values['ratio'] >= DelugeHandler.REQUIRED_RATIO):

            self.client.call('core.remove_torrent', torrent_hash, False)
            print(f'{torrent_values["name"]} is removed from seeding!')

            return True

        return False

    def _get_client_method_list(self) -> List[str]:
        """
        Returns a list of possible Deluge commands.
            Returns:
                (List[str]): A list of possible Deluge commands
        """
        return self.client.daemon.get_method_list()

    def _dump_torrents_to_json(self, torrents_to_filter: Dict[str, List[str]] = None,
                               stats_to_query: List[str] = None) -> None:
        """
        Saves the torrent hashes and stats to a JSON file.
            Parameters:
                torrents_to_filter (Dict[str, List[str]]): Dictionary which can be used to filter torrents by hash
                stats_to_query (List[str]): List of strings which can be used to filter torrent statistics
        """
        torrents = self.get_torrent_list(torrents_to_filter, stats_to_query)
        json.dump(torrents, Path('torrent_list.json').open('w', encoding='utf8'), indent=4)
