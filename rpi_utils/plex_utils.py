import json
from pathlib import Path
from typing import Tuple

from plexapi.server import PlexServer


class PlexHandler:
    """
    Class to connect to the locally running Plex server and read statistics from it.
    """
    def __init__(self) -> None:
        url, token = PlexHandler.read_config()
        self.plex = PlexServer(url, token)

    @staticmethod
    def read_config() -> Tuple[str, str]:
        """
        Reads the url and token from a config file.
            Returns:
                (Tuple[str, str]): Url and token strings for the locally hosted Plex server
        """
        config_file_path = Path(__file__).parents[1] / "config" / "config.json"
        config_dict = json.load(config_file_path.open(encoding="utf-8"))

        return config_dict["url"], config_dict["token"]

    def get_stats(self) -> dict:
        """
        Gathers statistics about the locally hosted Plex server.
            Returns:
                (dict): Dictionary that contains Plex statistics
        """
        return {"session_count": len(self.plex.sessions())}
